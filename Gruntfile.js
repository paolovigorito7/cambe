module.exports = function(grunt) {

    var sources_js = [
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        'app/_src/js/app.js',
    ];

    var devTasks = ['copy', 'sass:dev', 'postcss:dev', 'concat:dev', 'uglify:dev'];
    var buildTasks = ['copy', 'sass:dist', 'postcss:dist', 'concat:dist', 'uglify:dist'];

    grunt.initConfig({

        copy: {

            bootstrap_fonts: {
                cwd: 'bower_components/bootstrap-sass/assets/fonts/bootstrap/',
                src: '*.*',
                dest: 'app/_assets/fonts/bootstrap/',
                expand: true,
                flatten: false
            },
            icomoon_font: {
                cwd: 'app/_src/fonts/icomoon/fonts',
                src: '*.*',
                dest: 'app/_assets/fonts/icomoon/',
                nonull: false,
                expand: true,
                flatten: true
            },
            icomoon_style: {
                src: 'app/_src/fonts/icomoon/style.css',
                dest: 'app/_assets/fonts/icomoon/icomoon.css',
                nonull: true,
                expand: false,
                flatten: true
            },
            lodash: {
                cwd: 'bower_components/lodash/dist/',
                src: 'lodash.min.js',
                dest: 'app/_src/js/vendor/lodash/',
                expand: true,
                flatten: false
            },
        },

        sass: {
            options: {
                noCache: true,
                trace: true,
                precision: 5,
                noCache: true,
            },
            dev: {
                options: {
                    style: 'nested',
                    sourcemap: "auto",
                },
                files: {
                    'app/_assets/css/style.min.css': 'app/_src/scss/style.scss'
                }
            },
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: false,
                },
                files: {
                    'app/_assets/css/style.min.css': 'app/_src/scss/style.scss'
                }
            }
        },

        postcss: {
            dev: {
                options: {
                    map: true, // inline sourcemaps
                    processors: [
                        require('autoprefixer')({browsers: ['last 4 versions']}), // add vendor prefixes
                        require('pixrem')(), // add fallbacks for rem units
                    ]
                },
                src: 'app/_assets/css/style.min.css'
            },
            dist: {
                options: {
                    map: false,
                    processors: [
                        require('autoprefixer')({browsers: ['last 4 versions']}), // add vendor prefixes
                        require('pixrem')(), // add fallbacks for rem units
                    ]
                },
                src: 'app/_assets/css/style.min.css'
            }
        },

        concat: {
            dev: {
                options: {
                    sourceMap: false,
                    sourceMapName: 'app/_assets/js/scripts.map'
                },
                src: sources_js,
                dest: 'app/_assets/js/scripts.js'
            },
            dist: {
                options: {
                    sourceMap: false,
                    separator: ';'
                },
                src: sources_js,
                dest: 'app/_assets/js/scripts.min.js'
            }
        },

        uglify: {
            dev: {
                options: {
                    beautify: true,
                    preserveComments: true,
                    sourceMap: false,
                    mangle: true
                },
                files: {
                    'app/_assets/js/scripts.min.js': 'app/_assets/js/scripts.js'
                }
            },
            dist: {
                options: {
                    beautify: false,
                    preserveComments: false,
                    sourceMap: false,
                    mangle: false
                },
                files: {
                    'app/_assets/js/scripts.min.js': 'app/_assets/js/scripts.min.js'
                }
            }
        },

        watch: {
            grunt: {files: ['Gruntfile.js']},
            sass: {
                files: 'app/_src/scss/**/*.scss',
                tasks: ['sass:dev'],
                options: {
                    livereload: false
                }
            },
            js: {
                files: ['app/_assets/js/**/*.js', 'app/_src/js/**/*.js'],
                tasks: ['concat:dev', 'uglify:dev'],
                options: {
                    livereload: false
                }
            },
            all: {
                files: '**/*.php',
                options: {
                    livereload: false
                }
            }
        },

    });

    grunt.event.on('watch', function (action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' - ' + action);
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-postcss');

    // Register tasks
    grunt.registerTask('default',  ['copy', 'sass', 'postcss', 'concat', 'uglify', 'watch']);
    grunt.registerTask('dev', devTasks);
    grunt.registerTask('build', buildTasks);

};