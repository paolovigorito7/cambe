<?php

class Git {
  
  public function current_branch() {
    $cmd = exec( 'git branch | grep "*" ' );
    $cmd = str_replace( "* ", "", $cmd );
    return $cmd;
  }
  
  public function last_log() {
    $cmd    = FALSE;
    $string = 'git log --oneline -' . 1;
    if (!is_null( $string )) {
      $cmd = exec( $string );
    }
    return $cmd;
  }
  
  public function get_commit_hash($filename) {
    $cmd = exec( 'git log -n 1 --pretty=format:%h ' . $filename );
    return $cmd;
  }
  
  public function get_commit_author_name($filename) {
    $cmd = exec( 'git log -n 1 --pretty=format:%cn ' . $filename );
    return $cmd;
  }
  
  public function get_commit_relative_date($filename) {
    $cmd = exec( 'git log -n 1 --pretty=format:%cr ' . $filename );
    return $cmd;
  }
  
  public function get_commit_timestamp_date($filename) {
    $cmd = exec( 'git log -n 1 --pretty=format:%ct ' . $filename );
    return $cmd;
  }
  
  public function show_branches($local = TRUE) {
    $cmd = passthru( 'git branch' );
    return $cmd;
  }
  
}


class Project {
  
  public $name = 'CAMBE';
  
  public $app_root = 'app/';
  
  public $filters = [
    'pages'      => [ 'page-home_page.php' ],
    'navigation' => [
      'page-home_page.php',
      'module-navigation-std.php',
    ],
  ];
  
  public $cambe_list = [
    'contexts',
    'areas',
    'modules',
    'blocks',
    'elements',
    'pages',
    'templates',
    'resources',
    'widget',
  ];
  
  protected $disallowed_types = [
    '.',
    '..',
    '.DS_Store',
  ];
  
  protected $exclude_from_search = [];
  
  public function get_files() {
    $ritit = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $this->app_root, FilesystemIterator::SKIP_DOTS ), RecursiveIteratorIterator::CHILD_FIRST );
    $list  = [];
    foreach ($ritit as $splFileInfo) {
      if (!$splFileInfo->isDir() && !in_array( $splFileInfo->getFilename(), $this->disallowed_types )) {
        $list[] = [
          'filename'   => $splFileInfo->getFilename(),
          'path'       => $splFileInfo->getPath(),
          'cambe_type' => $this->get_cambe_type( $splFileInfo->getPath() ),
          'filters'    => $this->get_fe_filters( $splFileInfo->getFilename() ),
          'criteria'   => $this->get_fe_criteria( $splFileInfo ),
          'class'      => $this->get_fe_class( $splFileInfo->getFilename() ),
        ];
      }
    }
    return $list;
  }
  
  public function get_cambe_type($item) {
    $cambe = end( explode( '/', str_replace( [
      $this->app_root,
      '_',
    ], '', $item ) ) );
    if (!in_array( $cambe, $this->cambe_list )) {
      $cambe = 'resources';
    }
    return $cambe;
  }
  
  public function get_fe_filters($item) {
    foreach ($this->filters as $label => $files) {
      foreach ($files as $single_file) {
        if ($single_file == $item) {
          $filterable[] = $label;
        }
      }
    }
    if ($filterable != '') {
      $export = implode( ',', $filterable );
    }
    return $export;
  }
  
  public function get_fe_criteria($item) {
    $git      = new Git;
    $export   = FALSE;
    $criteria = [
      // 'eq'        => $eq ,
      'alphabeth' => substr( $item->getFilename(), 0, 1 ),
      'unix_date' => $git->get_commit_timestamp_date( $item->getPath() ),
      'commit'    => $git->get_commit_hash( $item->getPath() ),
      'author'    => $git->get_commit_author_name( $item->getPath() ),
    ];
    return $criteria;
  }
  
  public function get_fe_class($item) {
    $class = [];
    if (!in_array( $item, $this->exclude_from_search )) {
      $class[] = 'searchIn';
    }
    $export = implode( ' ', $class );
    return $export;
  }
  
  public function append_filters($item) {
    $export = FALSE;
    if ($item['filters']) {
      $export = 'data-filters="' . $item['filters'] . '"';
    }
    return $export;
  }
  
  public function append_criteria($item) {
    $export = FALSE;
    if ($item['criteria'] != '') {
      foreach ($item['criteria'] as $label => $value) {
        $attribute[] = 'data-' . $label . '="' . $value . '"';
      }
      if ($attribute != '') {
        $export = implode( ' ', $attribute );
      }
      
    }
    return $export;
  }
  
  public function append_url($item) {
    $url = $item['path'] . '/' . $item['filename'];
    return $url;
  }
  
  public function append_cambe_type($item) {
    if ($item['cambe_type']) {
      return 'data-cambe_type="' . $item['cambe_type'] . '"';
    }
  }
  
  public function show_filters($item) {
    $export = '';
    if ($item['filters']!= '') {
        $list = explode( ',', $item['filters']);
        foreach ($list as $key => $value) {
          $export .= '<small class="badge badge-light ' . 'filter-' . $value . '">' . $value . '</small> ';
        }
    }
    return $export;
  }
  
  public function show_cambe_type($item) {
    $export = false;
    if ($item['cambe_type']!= '') {
        $export = '<small class="badge badge-pill badge-light ' . 'cambe_type-' . $item['cambe_type'] . ' text-uppercase">' . substr( $item['cambe_type'], 0, 1) . '</small> ';
    }
    return $export;
  }
  
  public function show_criteria($item) {
    
    return $item;
  }
  
  public function get_npm_package() {
    $json_url = "package.json";
    $json     = file_get_contents( $json_url );
    $data     = json_decode( $json, TRUE );
    return $data;
  }
  
}

$git = new Git;
$project = new Project;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title><?php echo $project->name ?></title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        :focus {
            outline: 0;
        }

        body {
            overflow-y: scroll;
        }

        header .navbar-bottom .navbar-search > div:first-child {
            float: right;
            width: 80%;
        }

        .searchIn a {
            display: block;
            text-align: left;
        }

        .searchIn a:hover, .searchIn a:focus {
            text-decoration: underline;
            outline: 0;
        }

        .searchIn a:hover .label.label-info,
        .searchIn a:focus .label.label-info {
            text-decoration: none;
        }

        .searchIn.hidden {
            display: none;
            visibility: hidden;
        }

        .table-striped.-alt > tbody > tr {
            background-color: #f9f9f9;
        }

        .table-striped.-alt > tbody > tr.even {
            background-color: #fff;
        }

        .btn.btn-secondary {
            display: inline-block;
            line-height: 1;
            min-height: 32px;
        }

        .terminal {
            font-family: monospace;
            display: block;
            padding: 2px 8px;
            margin-bottom: 1rem;
            font-size: 90%;
            color: #ffffff;
            background-color: #373737;
            border: 1px solid #191919;
            border-radius: 3px;
            -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.25);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.25);
        }

        .text-monospace {
            font-family: monospace;
        }

        a.filter-active {
            border-color: white;
            color: white;
            background-color: black !important;
        }

        a.filter-active:hover, a.filter-active:focus {
            border-color: white;
            color: white;
            background-color: black !important;
        }

        pre.modal-body {
            margin-bottom: 0;
        }

        .modal-backdrop {
            background-color: #fff;
        }

        .modal-backdrop.show {
            opacity: .9;
        }

        .modal-content {
            border-color: rgba(0, 0, 0, 0.4);
            border-radius: 0;
        }

        @media (min-width: 992px) {
            .modal-lg {
                max-width: 94%;
            }
        }
    </style>
</head>
<body class="mt-5 pt-4">
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="https://www.digitalmill.it/wp-content/uploads/2015/12/mill.png"> <?php echo $project->name ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="btn btn-link btn-sm" data-content="" href="app-styleguide.html">
                    styleguide </a>
            </li>
            <li class="nav-item">
                <a class="btn btn-link btn-sm goto-modal" data-content=".local_branches" href="#!">
                    local branches </a>
            </li>
            <li class="nav-item">
                <a class="btn btn-link btn-sm goto-modal" data-content=".package" href="#!">
                    package </a>
            </li>
        </ul>
        <form class="form-inline form-search-box my-2 my-lg-0" method="GET">
            <div class="form-group ">
                <input type="text" id="searchFile" class="form-control mr-sm-2" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-outline-success my-2 my-sm-0 form-search-box-submit">
                Search
            </button>
        </form>
    </div>
</nav>
<div id="main_content" class="container">
    <div class="terminal row">
        <p class="mb-0">
          <?php echo $git->current_branch(); ?>
        </p>
    </div>
    <div class="terminal row">
        <p class="mb-0">
          <?php echo $git->last_log(); ?>
        </p>
    </div>
    <div class="filter-list-container row">
        <ul class="filters-list list-inline">
          <?php foreach ($project->cambe_list as $cambe) : ?>
              <li class="list-inline-item">
                  <a class="<?php echo 'badge badge-pill badge-light ' . 'filter-' . $cambe . ' ' . 'text-uppercase' ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $cambe ?>" data-cambe_filter="<?php echo $cambe ?>" href="<?php echo '#filter-' . $cambe ?>">
                    <?php echo substr( $cambe, 0, 1) ?>
                  </a>
              </li>
          <?php endforeach; ?>
          <?php foreach ($project->filters as $filter_label => $filter_content) : ?>
              <li class="list-inline-item">
                  <a class="<?php echo 'badge badge-light ' . 'filter-' . $filter_label ?>" data-target_filter="<?php echo $filter_label ?>" href="<?php echo '#filter-' . $filter_label ?>">
                    <?php echo $filter_label ?>
                  </a>
              </li>
          <?php endforeach; ?>
            <li class="list-inline-item">
                <a class="<?php echo 'badge badge-light ' . 'filter-' . 'reset' ?>" data-target_filter="<?php echo 'reset' ?>" href="#reset">
                    reset filters </a>
            </li>
        </ul>
    </div>
    <div class="row">
        <table class="table table-striped table-listing">
            <thead>
            <tr class="info">
                <th class="col-filename">
                    <a href="#!" class="data-sorting" data-sort_criteria="alphabeth" data-sort_by="">filename</a>&nbsp;<span class="text-info"></span>
                </th>
                <th class="col-filename text-right">
                    <a href="#!" class="data-sorting" data-sort_criteria="commit" data-sort_by="">commit</a>
                </th>
                <th class="col-filename">
                    <a href="#!" class="data-sorting" data-sort_criteria="author" data-sort_by="">author</a>
                </th>
                <th class="col-date text-right text-muted" colspan="1">
                    <a href="#!" class="data-sorting" data-sort_criteria="unix_date" data-sort_by="">date</a>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if ($project->get_files() != '') : ?><?php foreach ($project->get_files() as $eq => $item) : ?>
                <tr class="<?php print_r( $item['class'] ) ?>" <?php print_r( $project->append_cambe_type( $item ) ) ?> <?php print_r( $project->append_filters( $item ) ) ?> <?php print_r( $project->append_criteria( $item ) ) ?>>
                    <td class="text-left text-normal">
                        <a href="<?php echo $project->append_url( $item ); ?>" target="_blank">
                          <?php print_r( $project->show_cambe_type( $item ) ) ?>
                          <span class="td-content"><?php print_r( $item['filename'] ); ?></span>
                          <?php print_r( $project->show_filters( $item ) ) ?>
                        </a>
                    </td>
                    <td class="text-right text-monospace">
                        <span>
                          <?php echo $git->get_commit_hash( $project->append_url( $item ) ) ?>
                        </span>
                    </td>
                    <td class="text-text text-monospace">
                        <span>
                          <?php echo $git->get_commit_author_name( $project->append_url( $item ) ) ?>
                        </span>
                    </td>
                    <td class="text-right text-muted text-monospace">
                        <span>
                          <?php echo $git->get_commit_relative_date( $project->append_url( $item ) ) ?>
                        </span>
                    </td>
                </tr>
            <?php endforeach; ?><?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<div id="other_content" class="container">
    <div class="local_branches">
        <?php print_r( $git->show_branches() ) ?>
    </div>
    <div class="package">
        <?php print_r( $project->get_npm_package() ) ?>
    </div>
</div>
<div id="main_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="main_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <pre class="modal-body"></pre>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script>
    function resetFilters() {
        $('.searchIn').show();
        $('.searchIn').removeClass('hidden');
        $('.searchIn').removeClass('even');
        $('.filters-list a').removeClass('filter-active');
        $('.table').removeClass('table-striped').removeClass('-alt');
        $('.table').addClass('table-striped');
        $('.col-filename span').text('');
        $('#searchFile').val('');
    }

    function resetSorting() {
        $('.table-listing').find('.searchIn').sort(function (a, b) {
            console.log(a, b, '...');
            // return + a.dataset.eq - +b.dataset.eq;
        });
    }

    function showResults(searchedText, results) {
        $('.col-filename span').text(results + 1 + ' risultati');
        $('.table').removeClass('table-striped').removeClass('-alt');
        $(".searchIn td a:not(:contains('" + searchedText + "'))").each(function (i, element) {
            $(this).closest('.searchIn').addClass('hidden');
        });
        $('.table').addClass('table-striped -alt');
        $('.table .searchIn:visible:even').addClass('even');
    }

    function sort(event, order_by, criteria) {
        var $table = $('.table-listing');
        var container = [];
        var rows = $table.find('.searchIn').get();

        if (order_by == 'desc') {
            order_by = 'asc';
        } else {
            order_by = 'desc';
        }

        $(event.target).attr('data-sort_by', order_by).removeClass('sort_by-asc sort_by-desc').addClass('sort_by-' + order_by);

        $('.searchIn').each(function (key) {
            container.push({
                'eq': key,
                'az': $(this).attr('date-alphabeth'),
                'item': $(this).attr('date-item'),
                'timestamp': $(this).attr('date-unix_date'),
            });
        });

        rows.sort(function (a, b) {
            var keyA = $(a).attr('data-' + criteria);
            var keyB = $(b).attr('data-' + criteria);
            var comparison = 0;
            if (order_by == 'desc') {
                if (keyA < keyB) {
                    comparison = 1;
                }
                if (keyA > keyB) {
                    comparison = -1;
                }
            }
            if (order_by == 'asc') {
                if (keyA < keyB) {
                    comparison = -1;
                }
                if (keyA > keyB) {
                    comparison = 1;
                }
            }
            return comparison;
        });
        $.each(rows, function (index, row) {
            $table.children('tbody').append(row);
        });
    }

    function filter(event) {
        var current_filter = $(event.target).attr('data-target_filter');
        if (current_filter != 'reset') {
            if ($('[data-filters*=' + current_filter + ']').length > 0) {
                $('.searchIn').hide();
                $('.searchIn[data-filters*=' + current_filter + ']').show();
                $('.filters-list a').removeClass('filter-active');
            }
        } else {
            resetFilters();
        }
    }

    function cambe_filter(event) {
        var current_filter = $(event.target).attr('data-cambe_filter');
        if (current_filter != 'reset') {
            if ($('[data-cambe_type*=' + current_filter + ']').length > 0) {
                $('.searchIn').hide();
                $('.searchIn[data-cambe_type*=' + current_filter + ']').show();
                $('.filters-list a').removeClass('filter-active');
            }
        } else {
            resetFilters();
        }
    }

    $('[data-toggle="tooltip"]').tooltip();
    
    $('.form-search-box').submit(function (event) {
        event.preventDefault();

        var searchedText = $('#searchFile').val();
        var results = 0;
        var allStuff = $('.searchIn').length - 1;

        resetFilters();

        $(".searchIn td a:contains('" + searchedText + "')").each(function (i, element, countResults) {
            results = i;
        });

        if (searchedText == '' && searchedText.length == 0) {
            resetFilters();
        } else if (searchedText.length > 0 && searchedText != '' && results > 0) {
            showResults(searchedText, results);
        }

    });

    $('.data-sorting').on('click', function (event) {
        event.preventDefault();
        var order_by = $(event.target).attr('data-sort_by');
        var criteria = $(event.target).attr('data-sort_criteria');
        sort(event, order_by, criteria);
    });

    $('[data-target_filter]').on('click', function (event) {
        event.preventDefault();
        resetFilters();
        filter(event);
        $(this).addClass('filter-active');
    });

    $('[data-cambe_filter]').on('click', function (event) {
        event.preventDefault();
        resetFilters();
        cambe_filter(event);
        $(this).addClass('filter-active');
    });

    $('.goto-modal').on('click', function (event) {
        $('#main_modal').modal('toggle', $(this));
    });

    $("#other_content").addClass('d-none');

    $('#main_modal').on('show.bs.modal', function (event) {
        var modal_content = $($(event.relatedTarget).attr('data-content')).html();
        $('#main_modal .modal-body').append(modal_content);
    });
    
    $('#main_modal').on('hide.bs.modal', function (event) {
        $('#main_modal .modal-body').html('');
    });
    
</script>
</body>
</html>
